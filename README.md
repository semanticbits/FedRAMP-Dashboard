# FedRAMP Dashboard [![Build Status](https://travis-ci.org/semanticbits/FedRAMP-Dashboard.svg?branch=develop)](https://travis-ci.org/semanticbits/FedRAMP-Dashboard) [![Code Climate](https://codeclimate.com/repos/5707129dced1dc0e6f00c1be/badges/503576dad2a978fffa6e/gpa.svg)](https://codeclimate.com/repos/5707129dced1dc0e6f00c1be/feed) [![Test Coverage](https://codeclimate.com/repos/5707129dced1dc0e6f00c1be/badges/503576dad2a978fffa6e/coverage.svg)](https://codeclimate.com/repos/5707129dced1dc0e6f00c1be/coverage) [![Issue Count](https://codeclimate.com/repos/5707129dced1dc0e6f00c1be/badges/503576dad2a978fffa6e/issue_count.svg)](https://codeclimate.com/repos/5707129dced1dc0e6f00c1be/feed)

## Install required tools
### Autoconf (for git)
```
wget http://ftp.gnu.org/gnu/autoconf/autoconf-2.69.tar.gz
tar xvfvz autoconf-2.69.tar.gz
cd autoconf-2.69
./configure
make
make install
```
### Gettext (for git)
```
wget http://ftp.gnu.org/gnu/gettext/gettext-0.10.40.tar.gz
tar -zxf gettext-0.10.40.tar.gz
cd gettext-0.10.40
./configure
make
make install
```
### Git
```
wget https://github.com/git/git/archive/v2.6.4.tar.gz -O git.tar.gz
tar -zxf git.tar.gz
cd git-2.6.4
make configure
./configure --prefix=/usr/local
make
make install
```
### Node.js
```
wget https://nodejs.org/dist/v0.10.25/node-v0.10.25.tar.gz
tar xvfvz node-v0.10.25.tar.gz
cd node-v0.10.25
./configure
make
make install
```
## Checkout code
```
mkdir -p /usr/local/<username>/application/
cd /usr/local/<username>/application/
git clone git@gitlab.com:semanticbits/FedRAMP-Dashboard.git
git checkout develop
```
## Run the application
### Development
To start the application I've added a script in package.json. You can call it using following command.
```
cd FedRAMP-Dashboard/software/FedRAMP
nohup npm run-script start > nohup.out &
```
### Production
#### Resolve project dependencies
```
cd FedRAMP-Dashboard/software/FedRAMP
npm install --production
```
Note: prestart target will resolve nodejs dependencies and then bower dependencies. No need to run separate target for bower dependencies. If
we want to resolve bower dependencies separately, then run below command.
```
cd FedRAMP-Dashboard/software/FedRAMP
bower install --production
```
#### Configure Apache Alias to deploy
```
vi /etc/apache2/sites-available/000-default.conf

Alias / "/usr/local/<username>/application/FedRAMP-Dashboard/software/FedRAMP/"                
<Directory "/usr/local/<username>/application/FedRAMP-Dashboard/software/FedRAMP/">            
                Options -FollowSymLinks                                                        
                AllowOverride None                                                             
                Require all granted                                                            
</Directory>
```
## Run the test cases
### Karma
```
cd FedRAMP-Dashboard/software/FedRAMP
nohup npm run-script test-single-run > nohup.out &
```
### Protractor
```
cd FedRAMP-Dashboard/software/FedRAMP
nohup npm run-script protractor > nohup.out &
```
## Generate jshint report
You can generate the jshint report using the following script in package.json
```
cd FedRAMP-Dashboard/software/FedRAMP
npm run-script jshint-report
```
## Files and importance
1. package.json - nodeJS root file and it has all dependencies and start-up scripts
2. bower.json - bower root file and it has all dependencies
3. karma.conf.js - Unit test cases configuration file
3. Gruntfile.js - Grunt tasks (jshint report)