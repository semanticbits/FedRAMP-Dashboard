'use strict';

describe('table service', function(){
    var tableService, metadataList, thenFunction, thenErrFunction, scope, dc;
    beforeEach(module('fedRamp'));
    scope = {};
    dc = {
        tableFilter: {}
    };
    beforeEach(inject(function ($injector) {
        thenFunction = undefined;
        thenErrFunction = undefined;
        tableService = $injector.get('tableService');
        metadataList = [
            { 'title': 'CSP Name', 'field': 'cspName', 'sortable': 'cspName', 'show': true, 'viewColumn': true, type: 'text' },
            { 'title': 'Service Name', 'field': 'serviceName', 'sortable': 'serviceName', 'show': true, 'viewColumn': true, type: 'text' },
            { 'title': 'Leverage ATO?','field': 'leverageAto','sortable': 'leverageAto', 'show': false, 'viewColumn': true, type: 'text' },
            { 'title': 'Authorization Date', 'field': 'authorizationDate', 'sortable': 'authorizationDate', 'show': false, 'viewColumn': true, type: 'date' },
            { 'title': 'Designation', 'field': 'designation', 'sortable': 'designation', 'show': false, 'viewColumn': true, type: 'text' }
        ];
    }));

    it('Test filterTable filtering for filter with space attributes', function () {
        var dc = {
            tableFilter: {'cspName': '2001 Group'}
        };
        var tableFilterForAPI = tableService.filterTable(false, false, dc, metadataList, scope);
        expect(tableFilterForAPI).toBeDefined();
        expect(tableFilterForAPI).toEqual({cspName: '2001 Group', $: ''});
        expect(scope.searchRow).toEqual('cspName:"2001 Group"');
    });

    it('Test filterTable filtering for filter with invalid filter', function () {
        var tableFilterForAPI = tableService.filterTable(false, false,
            {tableFilter: {'cspNames': '2001 Group'}}, metadataList, scope);
        expect(tableFilterForAPI).toEqual({cspNames: '2001 Group', $: ''});
        expect(scope.searchRow).toEqual('');
    });

    it('Test filterTable filtering for filter with date filter', function () {
        var tableFilterForAPI = tableService.filterTable(false, false,
            {tableFilter: {'authorizationDate': {day: 7, month: 4, year: 2016}}}, metadataList, scope);
        expect(tableFilterForAPI).toEqual({authorizationDate: '04/07/2016', $: ''});
        expect(scope.searchRow).toEqual('authorizationDate:04/07/2016');
    });

    it('Test filterTable filtering for filter with date filter of empty month', function () {
        var tableFilterForAPI = tableService.filterTable(false, false,
            {tableFilter: {'authorizationDate': {day: 7, year: 2016}}}, metadataList, scope);
        expect(tableFilterForAPI).toEqual({authorizationDate: '', $: ''});
        expect(scope.searchRow).toEqual('');
    });

    it('Test filterTable filtering for empty value', function () {
        var tableFilterForAPI = tableService.filterTable(false, false,
            {tableFilter: {cspName: ''}}, metadataList, scope);
        expect(tableFilterForAPI).toEqual({$: ''});
    });

    it('Test filterTable filtering with entireQuery doesn\'t having quotes and all fields query', function () {
        scope = {
            searchRow: 'cspName:Microhard query value'
        };
        var tableFilterForAPI = tableService.filterTable(false, false, dc, metadataList, scope);
        expect(tableFilterForAPI).toEqual({$: 'query value'});
        expect(scope.searchRow).toEqual('query value');
    });

    it('Test filterTable filtering with only all fields query', function () {
        scope = {
            searchRow: 'query value'
        };
        dc = {
            tableFilter: {cspName: 'sample'}
        };
        var tableFilterForAPI = tableService.filterTable(true, true, dc, metadataList, scope);
        expect(dc.tableFilter).toEqual({});
        expect(scope.searchRow).toEqual('');
        expect(tableFilterForAPI).toEqual({$: ''});
    });

    it('Test updateFilter', function () {
        var tableFilter = tableService.updateFilter('cspName:Microhard', metadataList);
        expect(tableFilter).toEqual({ cspName: 'Microhard' });
    });

    it('Test updateFilter with date query', function () {
        var tableFilter = tableService.updateFilter('authorizationDate:04/07/2016', metadataList);
        expect(tableFilter).toEqual({ authorizationDate: { day: 7, month: 4, year: 2016 } });
    });

    it('Test updateFilter with invalid date query', function () {
        expect(tableService.updateFilter('authorizationDate:invalidDate', metadataList)).toEqual({});
    });

    it('Test updateFilter with with only all fields query', function () {
        var tableFilter = tableService.updateFilter('query value', metadataList);
        expect(tableFilter).toEqual({});
    });

    it('Test updateFilter for invalid query key', function () {
        var entireQuery = 'cspNames:Microhard serviceName:"Service Name1"';
        var tableFilter = tableService.updateFilter(entireQuery, metadataList);
        expect(tableFilter).toBeDefined();
        expect(tableFilter).toEqual({serviceName:'Service Name1'});
    });

    it('Test updateFilter without entireQuery', function () {
        var entireQuery;
        var tableFilter = tableService.updateFilter(entireQuery, metadataList);
        expect(tableFilter).toBeDefined();
        expect(tableFilter).toEqual({});
    });
});
