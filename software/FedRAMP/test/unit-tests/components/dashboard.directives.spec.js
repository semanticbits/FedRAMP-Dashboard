'use strict';
describe('Unit testing dashboardDirectives', function() {
    var $compile,
        $scope,
        rootScope,
        $templateCache;

    beforeEach(module('fedRamp'));

    // Store references to $rootScope and $compile
    // so they are available to all tests in this describe block
    beforeEach(inject(function($rootScope, _$compile_, $httpBackend, _$templateCache_){
        // The injector unwraps the underscores (_) from around the parameter names when matching
        $compile = _$compile_;
        rootScope = $rootScope;
        $scope = $rootScope.$new();
        $templateCache = _$templateCache_;
        $templateCache.put('app/views/_table_data.html', 'app/views/_table_data.html');
        $httpBackend.whenGET('app/views/_table_data.html').respond($templateCache.get('app/views/_table_data.html'));
        $httpBackend.whenGET('app/i18n/messages-en.json').respond({ hello: 'World' });
    }));

    it('Testing ng-table-wrapper directive', inject(function ($httpBackend, $location) {
        var element = $compile('<table wrap-in-div="ng-table-wrapper"></table>')($scope);
        $scope.$digest();
        expect(element.parent()[0].outerHTML).toEqual('<div class="ng-table-wrapper"><table wrap-in-div="ng-table-wrapper" class="ng-scope"></table></div>');
    }));
});


