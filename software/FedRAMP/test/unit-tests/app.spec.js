'use strict';

describe('angular-ui-route', function() {

    var $rootScope, $state, $injector,$location,$templateCache, state_name = '/', $scope;
    var $httpBackend;

    beforeEach(function() {
        module('fedRamp');
        inject(function(_$rootScope_, _$state_, _$injector_, _$templateCache_,_$location_) {
            $rootScope  = _$rootScope_;
            $state      = _$state_;
            $injector   = _$injector_;
            $location = _$location_;
            $templateCache = _$templateCache_;
            $scope = $rootScope.$new();
            $httpBackend = $injector.get('$httpBackend');
        });
        $templateCache.put('app/views/_table_data.html', 'app/views/_table_data.html');
        $templateCache.put('app/views/dashboard.html', 'app/views/dashboard.html');
        $templateCache.put('app/views/_table_data.html', 'app/views/_table_data.html');
        $httpBackend.whenGET('app/views/_table_data.html').respond($templateCache.get('app/views/_table_data.html'));
        $httpBackend.whenGET('app/i18n/messages-en.json').respond({ hello: 'World' });
    });
    describe('Testing success response ', function () {
        beforeEach(function() {
            $httpBackend.whenGET('https://api.github.com/repos/semanticbits/FedRAMP-Dashboard/contents/software/FedRAMP/data/P-ATO.csv?ref=develop').respond(
                'CSP Name,Service Name,Package ID,Website URL,Path,Leverage ATO?,Authorization Date,Authorizing Letter Date,Authorizing Letter Last Sign Date,Designation,Deployment Model,Service Model,Impact Level,Authorizing Agency,Sub-Agency,Authorizing Subagency,Authorizing Official,Expiration,Sponsoring Agency,Sponsoring Subagency,Sponsoring PackageID,SAP Date,SAR Date,SSP Date,Link to Authorizing Letter,Package Services Description,CFO Act Agency?,CSP POC,CSP POC Email,CSP Website,Agency POC,Agency POC Email,Independent Assessor,Next Annual Review,Last Annual  Review,Contingent On Letter?,Contingent Date,Contingency Met?,Contingency Description\n' +
                '2001 Group,Service Name1,X1393912199,www.url.com,CSP,Y,8/14/2015,1/2/2014,2/16/2014,In PMO Review,Hybrid Cloud,SaaS,Moderate,Small Business Administration,,,Fred Flintstone,None,,,,11/15/2013,11/16/2013,11/17/2013,www.letterurl1.com,Services description 1,Y,Bob Smith,Bob.Smith@2001Group.com,www.2001Group.com,Agency POC1,Agency.POC1@agencyname.gov,Blue Steel,1/2/2017,1/3/2016,N,,,\n' +
                'Microhard,ServiceName2,X1399937233,www.url1.com,Agency,N,7/9/2015,2/3/2014,3/20/2014,Compliant,Hybrid Cloud,SaaS,Moderate,Department of Health and Human Services,,Administration on Aging,Wilma Flintstone,7/9/2015,,,,12/9/2013,12/10/2013,12/11/2013,www.letterurl2.com,Services description 2,Y,John Doe,John.Doe@Microhard.com,www.Microhard.com,Agency POC2,Agency.POC2@agencyname.gov,Red Steel,1/2/2017,1/3/2016,N,6/16/2016,N,Need to paint system blue.\n' +
                'Amazone,ServiceName3,X1399937236,www.url2.com,Agency,Y,7/9/2015,5/13/2015,6/27/2015,Compliant,Hybrid Cloud,SaaS,Moderate,Department of Health and Human Services,,Agency for Healthcare Research and Quality,Wilma Flintstone,7/9/2018,Department of Defense,Air Force,X1399937000,1/2/2014,1/3/2014,1/4/2014,www.letterurl3.com,Services description 3,Y,Jane Brown,Jane.Brown@Amazone.com,www.Amazone.com,Agency POC3,Agency.POC3@agencyname.gov,Brown Steel,1/2/2017,1/3/2016,N,,,\n' +
                'Salesmerf,ServiceName4,X1399937239,www.url3.com,JAB,N,7/9/2015,5/13/2016,6/27/2016,Compliant,Hybrid Cloud,SaaS,Moderate,Department of Health and Human Services,,Health Resources and Services Administration,Wilma Flintstone,7/9/2018,,,,2/26/2014,2/27/2014,2/28/2014,www.letterurl4.com,Services description 4,Y,Paul Harvey,Paul.Harvey@Salesmerf.com,www.Salesmerf.com,Agency POC4,Agency.POC4@agencyname.gov,Black Steel,1/2/2017,1/3/2016,N,,,\n' +
                'Googleplex,ServiceName5,X1399937238,www.url4.com,JAB,Y,7/9/2015,1/1/2016,2/15/2016,Non Compliant,Hybrid Cloud,SaaS,Moderate,Department of Health and Human Services,,Indian Health Services,Wilma Flintstone,7/9/2018,,,,4/22/2014,4/23/2014,4/24/2014,www.letterurl5.com,Services description 5,Y,Jenn Early,Jenn.Early@Googleplex.com,www.Googleplex.com,Agency POC5,Agency.POC5@agencyname.gov,Yellow Steel,1/2/2017,1/3/2016,N,,,'
            );
        });

        function goTo(url) {
            $location.url(url);
            $rootScope.$digest();
        }

        describe('when empty, ', function () {

            beforeEach(function(){
                $templateCache.put('app/views/_table_data.html', 'app/views/_table_data.html');
            });
            it('should go to the home page', function () {
                goTo('');
                $httpBackend.flush();
                expect($state.current.name).toEqual('home');
            });
        });
    });
    describe('angular-ui-route with error response', function() {

        beforeEach(function() {
            $httpBackend.whenGET('https://api.github.com/repos/semanticbits/FedRAMP-Dashboard/contents/software/FedRAMP/data/P-ATO.csv?ref=develop').respond(500);
        });
        it('should go to the home page', function () {
            $location.url('/');
            $rootScope.$digest();
            $httpBackend.flush();
            expect($state.current.name).toEqual('home');
        });
    });
});

