'use strict';

describe('DashBoardController', function(){
    var httpBackend, $scope, rootScope, controller, $templateCache, timeOut, $location, iframeFocussed, iframePrinted;
    beforeEach(module('fedRamp'));
    beforeEach(module('controllers'));
    beforeEach(inject(function($controller, $rootScope, $httpBackend, _$templateCache_, $timeout,_$location_, $injector) {
        $scope = $rootScope.$new();
        rootScope = $rootScope;
        httpBackend=$httpBackend;
        $templateCache = _$templateCache_;
        $location = _$location_;
        iframeFocussed = false;
        iframePrinted = false;
        $templateCache.put('app/views/_table_data.html', 'app/views/_table_data.html');
        timeOut = $timeout;
        $httpBackend.whenGET('app/views/_table_data.html').respond($templateCache.get('app/views/_table_data.html'));
        $httpBackend.whenGET('app/i18n/messages-en.json').respond({ hello: 'World' });
        var utils = $injector.get('utils');
        var data =
            'CSP Name,Service Name,Package ID,Website URL,Path,Leverage ATO?,Authorization Date,Authorizing Letter Date,Authorizing Letter Last Sign Date,Designation,Deployment Model,Service Model,Impact Level,Authorizing Agency,Sub-Agency,Authorizing Subagency,Authorizing Official,Expiration,Sponsoring Agency,Sponsoring Subagency,Sponsoring PackageID,SAP Date,SAR Date,SSP Date,Link to Authorizing Letter,Package Services Description,CFO Act Agency?,CSP POC,CSP POC Email,CSP Website,Agency POC,Agency POC Email,Independent Assessor,Next Annual Review,Last Annual  Review,Contingent On Letter?,Contingent Date,Contingency Met?,Contingency Description\n' +
            '2001 Group,Service Name1,X1393912199,www.url.com,CSP,Y,8/14/2015,1/2/2014,2/16/2014,In PMO Review,Hybrid Cloud,SaaS,Moderate,Small Business Administration,,,Fred Flintstone,None,,,,11/15/2013,11/16/2013,11/17/2013,www.letterurl1.com,Services description 1,Y,Bob Smith,Bob.Smith@2001Group.com,www.2001Group.com,Agency POC1,Agency.POC1@agencyname.gov,Blue Steel,1/2/2017,1/3/2016,N,,,\n' +
            'Microhard,ServiceName2,X1399937233,www.url1.com,Agency,N,7/9/2015,2/3/2014,3/20/2014,Compliant,Hybrid Cloud,SaaS,Moderate,Department of Health and Human Services,,Administration on Aging,Wilma Flintstone,7/9/2015,,,,12/9/2013,12/10/2013,12/11/2013,www.letterurl2.com,Services description 2,Y,John Doe,John.Doe@Microhard.com,www.Microhard.com,Agency POC2,Agency.POC2@agencyname.gov,Red Steel,1/2/2017,1/3/2016,N,6/16/2016,N,Need to paint system blue.\n' +
            'Amazone,ServiceName3,X1399937236,www.url2.com,Agency,Y,7/9/2015,5/13/2015,6/27/2015,Compliant,Hybrid Cloud,SaaS,Moderate,Department of Health and Human Services,,Agency for Healthcare Research and Quality,Wilma Flintstone,7/9/2018,Department of Defense,Air Force,X1399937000,1/2/2014,1/3/2014,1/4/2014,www.letterurl3.com,Services description 3,Y,Jane Brown,Jane.Brown@Amazone.com,www.Amazone.com,Agency POC3,Agency.POC3@agencyname.gov,Brown Steel,1/2/2017,1/3/2016,N,,,\n' +
            'Salesmerf,ServiceName4,X1399937239,www.url3.com,JAB,N,7/9/2015,5/13/2016,6/27/2016,Compliant,Hybrid Cloud,SaaS,Moderate,Department of Health and Human Services,,Health Resources and Services Administration,Wilma Flintstone,7/9/2018,,,,2/26/2014,2/27/2014,2/28/2014,www.letterurl4.com,Services description 4,Y,Paul Harvey,Paul.Harvey@Salesmerf.com,www.Salesmerf.com,Agency POC4,Agency.POC4@agencyname.gov,Black Steel,1/2/2017,1/3/2016,N,,,\n' +
            'Googleplex,ServiceName5,X1399937238,www.url4.com,JAB,Y,7/9/2015,1/1/2016,2/15/2016,Non Compliant,Hybrid Cloud,SaaS,Moderate,Department of Health and Human Services,,Indian Health Services,Wilma Flintstone,7/9/2018,,,,4/22/2014,4/23/2014,4/24/2014,www.letterurl5.com,Services description 5,Y,Jenn Early,Jenn.Early@Googleplex.com,www.Googleplex.com,Agency POC5,Agency.POC5@agencyname.gov,Yellow Steel,1/2/2017,1/3/2016,N,,,';
        var content = {
            csv: data,
            header: true,
            separator: ',',
            id: true,
            camelCaseKey: true
        };
        //Converts the data into json object array
        var tableData = utils.csvToJSON(content);
        $location.url('/');
        rootScope.$digest();
        spyOn(document, 'getElementById').and.callFake(function(id) {
            return {
                contentWindow : {
                    focus: function() {
                        iframeFocussed = true;
                    },
                    print: function() {
                        iframePrinted = true;
                    }
                }
            };
        });

        controller = $controller('DashBoardController', { $scope: $scope, $timeout: timeOut, tableData: tableData });
        httpBackend.flush();
    }));
    it('Test DashBoardController', inject(function($controller) {
        expect(controller).toBeDefined();
        expect(controller.tableFilter).toEqual({});
        expect(controller.tableData[0].cspName).toEqual('2001 Group');
    }));
    it('Test filterData', inject(function($controller) {
        controller.filterTable();
        expect($scope.tableParams.tableFilter).toEqual({$: ''});
    }));
    it('Test filterData with sorting column', inject(function($controller) {
        $scope.tableParams.sorting({cspName: 'desc'});
        controller.filterTable();
        expect($scope.tableParams.tableFilter).toEqual({$: ''});
        expect($scope.tableParams.filteredData[0].cspName).toEqual('Salesmerf');
    }));
    it('Test updateTable', inject(function($controller) {
        controller.updateTable('Compliant');
        expect($scope.tableParams.tableFilter).toEqual({designation: 'Compliant', $: ''});
        expect($scope.tableParams.filteredData.length).toEqual(3);
    }));
    it('Test iframeLoaded', inject(function($controller) {
        controller.iframeLoaded();
    }));
    it('Test printTable', inject(function($controller) {
        controller.printTable();
        timeOut.flush();
    }));
    it('Test openDropdown', inject(function($controller) {
        controller.openDropdown({});
    }));
    it('Test openDropdown with hidden dropdown', inject(function($controller) {
        var addedClass = '';
        spyOn(jQuery.fn, 'parents').and.callFake(function(parentsSelector) {
            return{
                children: function (childrenSelector) {
                    return {
                        is: function(isQuery) {
                            return true;
                        },
                        toggle: function() {
                            return {
                                parents: function(parentsSelector) {
                                    return{
                                        children: function (childrenSelector) {
                                            return {
                                                addClass: function(classToAdd) {
                                                    addedClass = classToAdd;
                                                }
                                            };
                                        }
                                    };
                                }
                            };

                        }
                    };
                }
            };
        });
        controller.openDropdown({});
        expect(addedClass).toEqual('active');
    }));
});
describe('DashBoardController with error response', function(){
    var httpBackend, $scope, rootScope, controller, $templateCache;
    beforeEach(module('fedRamp'));
    beforeEach(module('controllers'));
    beforeEach(inject(function($controller, $rootScope, $httpBackend, _$templateCache_, $timeout,_$location_) {
        $scope = $rootScope.$new();
        rootScope = $rootScope;
        httpBackend=$httpBackend;
        $templateCache = _$templateCache_;
        $templateCache.put('app/views/_table_data.html', 'app/views/_table_data.html');
        $httpBackend.whenGET('app/views/_table_data.html').respond($templateCache.get('app/views/_table_data.html'));
        $httpBackend.whenGET('app/i18n/messages-en.json').respond({ hello: 'World' });

        controller = $controller('DashBoardController', { $scope: $scope, tableData: undefined});
        httpBackend.flush();
    }));
    it('Test DashBoardController with error response', inject(function($controller) {
        expect(controller).toBeDefined();
    }));
});

