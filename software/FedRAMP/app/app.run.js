'use strict';
(function() {
    angular
        .module('fedRamp')
        .run(appRun);
    appRun.$inject = ['$templateCache', '$http'];

    function appRun($templateCache, $http){
        //Put the header required templates in $templateCache
        $templateCache.put('ng-table/header.html', '<ng-table-sorter-row></ng-table-sorter-row>');
        $templateCache.put('ng-table/sorterRow.html', '<tr class="ng-table-sort-header"> <th title="{{$column.headerTitle(this)}}" ng-repeat="$column in $columns" ng-class="{ \'sortable\': $column.sortable(this), \'sort-asc\': params.sorting()[$column.sortable(this)]==\'asc\', \'sort-desc\': params.sorting()[$column.sortable(this)]==\'desc\' }" ng-click="sortBy($column, $event)" ng-if="$column.show(this)" ng-init="template=$column.headerTemplateURL(this)" class="header {{$column.class(this)}}"> <div ng-if="!template" class="ng-table-header" ng-class="{\'sort-indicator\': params.settings().sortingIndicator==\'div\'}"> <div ng-bind="$column.title(this)" ng-class="{\'sort-indicator\': params.settings().sortingIndicator==\'span\'}"></div> </div> <div ng-if="template" ng-include="template"></div> </th> </tr> ');
        $http.get('app/views/_table_data.html', { cache: true }).success(function (html) {
            $templateCache.put('app/views/_table_data.html', html);
        });
    }
}());