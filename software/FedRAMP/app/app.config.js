'use strict';
(function() {
    angular
        .module('fedRamp')
        .config(appConfig);
    appConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider', '$translateProvider'];
    function appConfig($stateProvider, $urlRouterProvider, $locationProvider, $translateProvider) {
        $stateProvider.state('home', {
            url:'/',
            views:{
                home:{
                    templateUrl: 'app/views/dashboard.html',
                    controller: 'DashBoardController',
                    controllerAs: 'dc',
                    resolve: {
                        tableData: tableData
                    }
                }
            }
        });
        //if no url, redirect to home page
        $urlRouterProvider.otherwise('/');
        // use the HTML5 History API
        $locationProvider.html5Mode(true);
        // configures staticFilesLoader
        $translateProvider.useStaticFilesLoader({
            prefix: 'app/i18n/messages-',
            suffix: '.json'
        });
        // load 'en' table on startup
        $translateProvider.preferredLanguage('en');
        $translateProvider.useSanitizeValueStrategy('sanitize');

    }
    tableData.$inject = ['tableService'];
    function tableData(tableService) {
        return tableService.getDataForTable();
    }

}());