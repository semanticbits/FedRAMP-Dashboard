'use strict';
(function() {
    angular
        .module('directives', []);
    angular
        .module('directives')
        .directive('wrapInDiv', wrapInDiv);
    /**
     * Used to wrap the table in a div with given class
     */
    function wrapInDiv() {
        var directive = {
            priority: 1002,
            link: link,
            restrict: 'A'
        };

        return directive;

        function link(scope, element, attrs) {
            var wrapper = angular.element('<div>');
            wrapper.addClass(attrs.wrapInDiv);
            element.wrap(wrapper);
        }
    }
}());
