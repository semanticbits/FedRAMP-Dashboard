'use strict';
(function(){
    angular
        .module('services')
        .factory('utils', utils);
    utils.$inject = ['$filter'];
    function utils($filter){
        var service = {
            toCamelCase: toCamelCase,
            isDateString: isDateString,
            csvToJSON: csvToJSON,
            findByKeyAndValue: findByKeyAndValue,
            findAllByKeyAndValue: findAllByKeyAndValue,
            findAndUpdateValueInAttr: findAndUpdateValueInAttr,
            findStatusInfo: findStatusInfo,
            isValueNotEmpty: isValueNotEmpty
        };
        return service;

        /**
         * Converts given string to camel case.
         * @param s
         * @returns {string|*}
         */
        function toCamelCase(s) {
            // remove all characters that should not be in a variable name
            // as well underscores an numbers from the beginning of the string
            s = s.replace(/([^a-zA-Z0-9_\- ])|^[_0-9]+/g, '').trim().toLowerCase();
            // uppercase letters preceeded by a hyphen or a space
            s = s.replace(/([ -]+)([a-zA-Z0-9])/g, function(a,b,c) {
                return c.toUpperCase();
            });
            // uppercase letters following numbers
            s = s.replace(/([0-9]+)([a-zA-Z])/g, function(a,b,c) {
                return b + c.toUpperCase();
            });
            return s;
        }

        /**
         * Checks whether the given string is a date.
         * @param value
         * @returns {boolean}
         */
        function isDateString(value) {
            return !isNaN(Date.parse(value));
        }

        /**
         * converts the given csv to json
         * @param content
         * @returns {{headers: Array, json: Array}}
         */
        function csvToJSON(content) {
            var lines=content.csv.split('\n');
            var result = [];
            var start = 0;
            var columnCount = lines[0].split(content.separator).length;

            var headers = [];
            if (content.header) {
                angular.forEach(lines[0].split(content.separator), function(eachHeader, index) {
                    headers.push({
                        title: eachHeader,
                        type: 'text'
                    });
                });
                start = 1;
            }

            for (var i=start; i<lines.length; i++) {
                var obj = {};
                if(content.id) {
                    obj.id = i;
                }
                var currentline=lines[i].split(new RegExp(content.separator+'(?![^"]*"(?:(?:[^"]*"){2})*[^"]*$)'));
                if ( currentline.length === columnCount ) {
                    if (content.header) {
                        for (var j=0; j<headers.length; j++) {
                            var key = content.camelCaseKey ? toCamelCase(headers[j].title) : headers[j].title;
                            if(isDateString(currentline[j])) {
                                headers[j].type = 'date';
                                obj[key] = $filter('date')(new Date(currentline[j]), 'MM/dd/yyyy');
                            } else {
                                obj[key] = currentline[j];
                            }
                        }
                    } else {
                        for (var k=0; k<currentline.length; k++) {
                            obj[k] = currentline[k];
                        }
                    }
                    result.push(obj);
                }
            }
            return {headers: headers, json: result};
        }

        /**
         * Finds and returns the first object in array of objects by using the key and value
         * @param a
         * @param key
         * @param value
         * @returns {*}
         */
        function findByKeyAndValue(a, key, value) {
            for (var i = 0; i < a.length; i++) {
                if (a[i][key] === value) {return a[i];}
            }
            return null;
        }

        /**
         * Finds and returns all objects in array of objects by using the key and value
         * @param a
         * @param key
         * @param value
         * @returns {Array}
         */
        function findAllByKeyAndValue(a, key, value) {
            var result = [];
            for (var i = 0; i < a.length; i++) {
                if (a[i][key] === value) {
                    result.push(a[i]);
                }
            }
            return result;
        }

        /**
         * Finds and updates the first object in array of objects by using the findKey and findValue
         * @param a
         * @param findKey
         * @param findValue
         * @param propertyKey
         * @param propertyValue
         * @returns {boolean}
         */
        function findAndUpdateValueInAttr(a, findKey, findValue, propertyKey, propertyValue) {
            var data = findByKeyAndValue( a, findKey, findValue );
            if( !angular.isUndefined(data) && data !==null  ){
                data[propertyKey] = propertyValue;
                return true;
            }
            return false;
        }

        /**
         * Finds the information related to status
         * @param a
         * @param key
         * @returns {Array}
         */
        function findStatusInfo(a, key) {
            var result = [];
            for (var i = 0; i < a.length; i++) {
                if ( result.length === 0  || findByKeyAndValue(result,'name',a[i][key]) === null ) {
                    var data = {name:a[i][key], count:1};
                    result.push(data);
                }else {
                    var statusInfo = findByKeyAndValue(result,'name',a[i][key]);
                    statusInfo.count += 1;
                }
            }
            return result;
        }

        /**
         * Util for finding a value is empty or not
         * @param value
         * @returns {boolean|*}
         */
        function isValueNotEmpty(value) {
            return angular.isDefined(value) && value !== null &&
                (angular.isDate(value) || angular.isNumber(value) || typeof value === 'boolean' || !jQuery.isEmptyObject(value));
        }
    }
}());