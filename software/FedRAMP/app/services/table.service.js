'use strict';
(function(){
    angular
        .module('services', []);
    angular
        .module('services')
        .factory('tableService', tableService);
    tableService.$inject = ['utils', '$http', '$q', '$filter'];
    function tableService(utils, $http, $q, $filter) {
        var service = {
            getDataForTable: getDataForTable,
            updateFilter: updateFilter,
            filterTable: filterTable
        };
        return service;

        function getDataForTable() {
            return $http({
                method: 'GET',
                //url: 'https://api.github.com/repos/18F/fedramp-micropurchase/contents/P-ATO.csv?ref=master',
                url: 'https://api.github.com/repos/semanticbits/FedRAMP-Dashboard/contents/software/FedRAMP/data/P-ATO.csv?ref=develop',
                headers: {
                    'Accept': 'application/vnd.github.VERSION.raw',
                    'Content-Type': 'application/json'
                }
            }).then(getDataForTableComplete, getDataForTableError);

            function getDataForTableComplete(response) {
                var content = {
                    csv: response.data,
                    header: true,
                    separator: ',',
                    id: true,
                    camelCaseKey: true
                };
                //Converts the data into json object array
                return utils.csvToJSON(content);
            }

            function getDataForTableError(error) {
                console.error('Error occured while loading data.', error);
                $q.reject(error);
            }
        }

        /**
         * Parses the query given in complete search box
         * Sample query = designation:"In PMO Review" cspName:2001 Test
         * @param textQuery
         * @returns {{}}
         */
        function parseQuery(textQuery) {
            var jsonQuery = {};
            var key, value;
            if(angular.isDefined(textQuery)) {
                while ((textQuery = textQuery.trim()) !== '') {
                    if (textQuery.indexOf(':') >= 0) {
                        key = textQuery.substr(0, textQuery.indexOf(':'));
                        textQuery = textQuery.substr(textQuery.indexOf(':') + 1, textQuery.length);
                        //If the value starts with " then capture the value until the end of "
                        if (textQuery.indexOf('"') === 0) {
                            textQuery = textQuery.substr(1, textQuery.length);
                            value = textQuery.substr(0, textQuery.indexOf('"'));
                            textQuery = textQuery.substr(textQuery.indexOf('"') + 1, textQuery.length);
                        } else {
                            //If the value doesn't starts with "  and it contains space then capture the value until the space
                            if(textQuery.indexOf(' ') >= 0) {
                                value = textQuery.substr(0, textQuery.indexOf(' '));
                                textQuery = textQuery.substr(textQuery.indexOf(' ') + 1, textQuery.length);
                            } else {
                                //If the value doesn't starts with "  and it doesn't contains space then capture the complete value
                                value = textQuery;
                                textQuery = '';
                            }
                        }
                        jsonQuery[key] = value;
                    } else {
                        textQuery = '';
                    }
                }
            }
            return jsonQuery;
        }

        /**
         * Parses the common query given in complete search box
         * @param textQuery
         * @returns {string}
         */
        function parseCommonQuery(textQuery) {
            var commonQuery = '';
            if(angular.isDefined(textQuery)) {
                //If query doesn't contain : then the query is the common query
                if(textQuery.indexOf(':') < 0) {
                    commonQuery = textQuery;
                } else {
                    //Take the last value for splits of :
                    commonQuery = textQuery.split(':').pop();
                    //If the value starts with " then capture the value from the end of "
                    if (commonQuery.indexOf('"') === 0) {
                        commonQuery = commonQuery.substr(1, commonQuery.length);
                        commonQuery = commonQuery.substr(commonQuery.indexOf('"') + 1, commonQuery.length);
                    } else if(commonQuery.indexOf(' ') >= 0) {
                        //If the value doesn't starts with " and contains space in it, then capture the value from the next space
                        commonQuery = commonQuery.substr(commonQuery.indexOf(' ') + 1, commonQuery.length);
                    } else {
                        commonQuery = '';
                    }
                }
            }
            return commonQuery.trim();
        }

        /**
         * Prepares the entire query given in complete search box
         * @param tableFilterForSearch
         * @param metadataList
         * @returns {string}
         */
        function prepareEntireQuery(tableFilterForSearch, metadataList) {
            var entireQuery = '';
            angular.forEach(tableFilterForSearch, function(value, key) {
                if(utils.isValueNotEmpty(value)) {
                    var metadata = utils.findByKeyAndValue(metadataList, 'field', key);
                    if (utils.isValueNotEmpty(metadata)) {
                        entireQuery += key + ':';
                        entireQuery += value.indexOf(' ') >= 0 ? '"' + value + '"' : value;
                        entireQuery += ' ';
                    }
                }
            });
            return entireQuery;
        }

        /**
         * Updates the entire query given in complete search box into filter drop down
         * @param entireQuery
         * @param metadataList
         * @returns {{}}
         */
        function updateFilter(entireQuery, metadataList) {
            var tableFilter = {};
            angular.forEach(parseQuery(entireQuery), function(value, key) {
                var filterName = key;
                var metadata = utils.findByKeyAndValue(metadataList, 'field', filterName);
                if(utils.isValueNotEmpty(metadata)) {
                    if(metadata.type === 'date') {
                        if(utils.isDateString(value)){
                            var date = new Date(value);
                            tableFilter[filterName] = {};
                            tableFilter[filterName].day = date.getDate();
                            tableFilter[filterName].month = date.getMonth() + 1;
                            tableFilter[filterName].year = date.getUTCFullYear();
                        }
                    } else {
                        tableFilter[filterName] = value;
                    }
                }
            });
            return tableFilter;
        }

        function getValueForSearch(value) {
            var valueForSearch = '';
            if (angular.isObject(value)) {
                //converting the date range values to query the api
                if(utils.isValueNotEmpty(value.day) && utils.isValueNotEmpty(value.month) && utils.isValueNotEmpty(value.year)) {
                    valueForSearch = $filter('date')(new Date(value.year, value.month - 1, value.day), 'MM/dd/yyyy');
                }
            } else {
                valueForSearch = value;
            }
            return valueForSearch;
        }

        function tableQueryForSearch(tableFilter) {
            var filter = {};
            angular.forEach(tableFilter, function(value, key) {
                if(utils.isValueNotEmpty(value)) {
                    filter[key]= getValueForSearch(value);
                }
            });
            return filter;
        }

        /**
         * Prepares the filter object to search the data
         * @param dc
         * @param metadataList
         * @param scope
         */
        function prepareTableFilterForSearch(dc, metadataList, scope) {
            var tableFilterForSearch = tableQueryForSearch(angular.copy(dc.tableFilter));
            var entireQuery = prepareEntireQuery(angular.copy(tableFilterForSearch), metadataList);

            //Assign common search query to filter
            tableFilterForSearch.$ = parseCommonQuery(angular.copy(scope.searchRow));
            scope.searchRow = entireQuery + tableFilterForSearch.$;
            scope.searchRow = scope.searchRow.trim();
            return tableFilterForSearch;
        }

        /**
         * Filters the table by forming the query
         * @param reset
         * @param resetAll
         * @param dc
         * @param metadataList
         * @param scope
         */
        function filterTable(reset, resetAll, dc, metadataList, scope) {
            //Hides the drop-down when user hit search or cancel button
            jQuery('body').trigger('click');
            //If th user clicks cancel button, then set the tableFilter as empty
            if (reset) {
                dc.tableFilter = {};
            }

            //If th user clicks cancel button, then set the tableFilter as empty
            if(resetAll) {
                scope.searchRow = '';
            }
            return prepareTableFilterForSearch(dc, metadataList, scope);
        }
    }
}());