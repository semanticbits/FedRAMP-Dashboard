'use strict';
jQuery(document).ready(function (e) {
    e(document).bind('click', function (t) {
        var n = e(t.target);
        if (!n.parents().hasClass('button-dropdown')){ e('.button-dropdown .dropdown-menu').hide(); }
    });
    e(document).bind('click', function (t) {
        var n = e(t.target);
        if (!n.parents().hasClass('button-dropdown')) { e('.button-dropdown .dropdown-toggle').removeClass('active'); }
    });
});