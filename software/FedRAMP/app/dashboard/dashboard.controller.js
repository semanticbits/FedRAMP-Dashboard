'use strict';
(function() {
    angular
        .module('controllers', [])
        .controller('DashBoardController',DashBoardController);
    DashBoardController.$inject = [
        '$scope', 'utils', 'NgTableParams', 'tableService', '$filter', '$compile', '$templateCache','$timeout',
        '$window','tableData'
    ];
    function DashBoardController($scope, utils, NgTableParams, tableService, $filter, $compile, $templateCache, $timeout, $window, tableData) {
        var dc = this;
        dc.tableData = [];
        dc.tableMetadataList = [];
        dc.defaultColumns = ['CSP Name', 'Service Name', 'Package ID', 'Path', 'Designation'];
        dc.tableFilter = {};
        dc.activeColumns = utils.findAllByKeyAndValue(dc.tableMetadataList, 'show', true).length;
        dc.openDropdown = openDropdown;
        dc.updateTable = updateTable;
        dc.getAllData = getAllData;
        dc.addCSSLinkToIframe = addCSSLinkToIframe;
        dc.addTitleToIframe = addTitleToIframe;
        dc.iframeLoaded = iframeLoaded;
        dc.printPage = printPage;
        dc.printTable = printTable;
        dc.filterTable = filterTable;
        dc.phone = false;

        activate();

        /**
         * Opens the dropdown
         */
        function openDropdown(event) {
            var t = jQuery(event.currentTarget)
                .parents('.button-dropdown')
                .children('.dropdown-menu')
                .is(':hidden');
            jQuery('.button-dropdown .dropdown-menu')
                .hide();
            jQuery('.button-dropdown .dropdown-toggle')
                .removeClass('active');
            if (t) {
                jQuery(event.currentTarget)
                    .parents('.button-dropdown')
                    .children('.dropdown-menu')
                    .toggle()
                    .parents('.button-dropdown')
                    .children('.dropdown-toggle')
                    .addClass('active');
            }
        }

        /**
         * Updates the table by filtering with given designation
         * @param designation
         */
        function updateTable( designation ) {
            dc.tableFilter = {designation: designation};
            $scope.tableParams.exactMatch = true;
            utils.findAndUpdateValueInAttr(dc.statusData, 'selected', true, 'selected', false);
            utils.findAndUpdateValueInAttr(dc.statusData, 'name', designation, 'selected', true);
            dc.filterTable();
        }

        /**
         * Gives the data after filtering
         * @returns {{total: *, data: (Array|Array.<T>|string|Blob|ArrayBuffer|*)}}
         */
        function getAllData() {
            var filteredData = angular.copy(dc.tableData);
            var page = $scope.tableParams.page();
            var count = $scope.tableParams.count();
            var filter = angular.copy($scope.tableParams.tableFilter);
            //If the exact match is required
            if($scope.tableParams.exactMatch) {
                $scope.tableParams.exactMatch = false;
                angular.forEach(filter, function(value, key) {
                    if(key !== '$') {
                        filteredData = utils.findAllByKeyAndValue(filteredData, key, value);
                    }
                });
            } else {
                utils.findAndUpdateValueInAttr(dc.statusData, 'selected', true, 'selected', false);
                filteredData = $filter('filter')(filteredData, filter);
            }
            var orderedData = utils.isValueNotEmpty($scope.tableParams.sorting()) ?
                $filter('orderBy')(filteredData, $scope.tableParams.orderBy()) :
                filteredData;
            $scope.tableParams.filteredData = orderedData;
            return {
                total: orderedData.length,
                data: orderedData.slice((page - 1) * count, page * count)
            };
        }

        /**
         * Adds css link to Iframe
         * @param theFrame
         * @param link
         */
        function addCSSLinkToIframe(theFrame, link ) {
            theFrame.contents().find('head').append($('<link/>', { rel: 'stylesheet', href: link, type: 'text/css', media:'all' }));
        }
        /**
         * Adds title to Iframe
         * @param theFrame
         * @param title
         */
        function addTitleToIframe(theFrame, title ) {
            theFrame.contents().find('html').attr('lang', 'en');
            theFrame.contents().find('title').remove();
            theFrame.contents().find('head').append($('<title/>', { text: title }));
        }

        /**
         * Executes after the iframe is loaded
         */
        function iframeLoaded(){
            var $iframe = $('#printf');
            dc.addTitleToIframe($iframe, 'P-ATO');
            dc.addCSSLinkToIframe($iframe, 'bower_components/ng-table/dist/ng-table.min.css');
            dc.addCSSLinkToIframe($iframe, 'node_modules/uswds/dist/css/uswds.min.css');
            dc.addCSSLinkToIframe($iframe, 'app/app.css');
        }

        /**
         * Prints the given html
         * @param html
         */
        function printPage( html ) {
            $('#printf').contents().find('body').html(html);
            document.getElementById('printf').contentWindow.focus();
            document.getElementById('printf').contentWindow.print();
        }

        /**
         * Prints the table
         */
        function printTable() {
            $scope.printableRows = angular.copy($scope.tableParams.filteredData);
            $scope.printableHeaders = utils.findAllByKeyAndValue(dc.tableMetadataList, 'show', true);
            var ele = angular.element('<div></div>');
            ele.html($templateCache.get('app/views/_table_data.html'));
            var compileEle = $compile(ele.contents())($scope);
            $timeout(function () { }, 1, true).then(function () {
                dc.printPage(compileEle.html());
            });
        }

        /**
         * Filters the table
         * @param reset - resets the dropdown form
         * @param resetAll - Reset common search area
         */
        function filterTable(reset, resetAll) {
            $scope.tableParams.tableFilter = angular.copy(tableService.filterTable(reset, resetAll, dc, dc.tableMetadataList, $scope));
            $scope.tableParams.parameters().page=1;
            $scope.tableParams.reload();
        }

        function loadData(result) {
            if(!utils.isValueNotEmpty(result)) {
                return;
            }
            dc.tableData = result.json;
            //Get the status information to display on the header
            dc.statusData = utils.findStatusInfo(dc.tableData, 'designation');
            angular.forEach(result.headers, function (header, index) {
                var field = utils.toCamelCase(header.title);
                var eachMetadata = {
                    title: header.title,
                    field: field,
                    sortable: field,
                    type: header.type,
                    show: dc.defaultColumns.indexOf(header.title) >= 0
                };
                dc.tableMetadataList.push(eachMetadata);
            });
            //Initializes the ng-table params
            $scope.tableParams = new NgTableParams({}, {
                getData: function ($defer, params) {
                    var result = dc.getAllData();
                    $scope.tableParams.total(result.total);
                    $defer.resolve(result.data);
                }
            });

            //Watches the change in searchRow and updates the dropdown filter
            $scope.$watch('searchRow', function (newValue, oldValue) {
                dc.tableFilter = tableService.updateFilter(newValue, dc.tableMetadataList);
            });
        }

        function activate() {
            jQuery('.dropdown-toggle').click(dc.openDropdown);
            $('#printf').on('load', dc.iframeLoaded);
            if ($window.matchMedia) {
                dc.phone = $window.matchMedia('screen and (max-width: 767px)').matches;
            }
            loadData(tableData);
        }
    }
}());