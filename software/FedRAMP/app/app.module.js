'use strict';
// Declare app level module which depends on views, and components
angular.module('fedRamp', [
    'ngAria',
    'ui.router',
    'controllers',
    'directives',
    'services',
    'ngTable',
    'ngResource',
    'ngSanitize',
    'pascalprecht.translate'
]);

