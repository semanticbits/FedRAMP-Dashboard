module.exports = function(config){
  config.set({

    basePath : './',

    files : [
      'bower_components/angular/angular.js',
      'bower_components/angular-route/angular-route.js',
      'bower_components/angular-aria/angular-aria.js',
      'bower_components/angular-mocks/angular-mocks.js',
      'bower_components/angular-sanitize/angular-sanitize.js',
      'bower_components/angular-ui-router/release/angular-ui-router.js',
      'bower_components/jquery/dist/jquery.js',
      'bower_components/ng-table/dist/ng-table.js',
      'bower_components/angular-translate/angular-translate.js',
      'bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.js',
      'bower_components/angular-resource/angular-resource.js',
      'node_modules/uswds/dist/js/uswds.min.js',
      'app/app.module.js',
      'app/**/*.js',
      'test/unit-tests/**/*.js'
    ],

    preprocessors: {
      'app/**/*.js': ['coverage'],
      'app/views/**/*.html': ['html2js']
    },

    autoWatch : true,

    frameworks: ['jasmine'],

    browsers : ['PhantomJS'],

      reporters : ['dots', 'junit', 'html','coverage'],

      plugins : [
          'karma-chrome-launcher',
          'karma-firefox-launcher',
          'karma-phantomjs-launcher',
          'karma-jasmine',
          'karma-junit-reporter',
          'karma-htmlfile-reporter',
          'karma-coverage',
          'karma-ng-html2js-preprocessor'
      ],

    junitReporter : {
      outputFile: 'reports/test_out/unit.xml',
      suite: 'unit'
    },
    htmlReporter: {
      outputFile: 'reports/test_out/units.html',

      // Optional
      pageTitle: 'Unit Tests',
      subPageTitle: 'A sample project description'
    },
    check: {
      global: {
          statements: 90,
          branches: 90,
          functions: 100,
          lines: 90
      },
      each: {
          statements: 90,
          branches: 90,
          functions: 100,
          lines: 90
      }
    },

    coverageReporter:{
      type : 'lcov',
      dir : 'reports/coverage/',
      file : 'index.html'
    },

    // add the plugin settings
    ngHtml2JsPreprocessor: {
      stripPrefix: ''
    }
  });
};
