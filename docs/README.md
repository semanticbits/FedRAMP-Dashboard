## Test Reports
This page shows various test reports produced by the application.

### Code Climate Reports
### Dashboard
![Code Climate Dashboard](https://raw.githubusercontent.com/semanticbits/FedRAMP-Dashboard/master/docs/reports/Codeclimate-Dashboard-wiht-GPA-Test-Coverage.png "Code Climate - Dashboard")

### FedRAMP Project Feed Page
![Code Climate Feed Page](https://raw.githubusercontent.com/semanticbits/FedRAMP-Dashboard/master/docs/reports/Codeclimate-Main-page-for-feed.png "Code Climate - FedRAMP project Feed Page")

### Test Coverage
![Code Climate Test Coverage](https://raw.githubusercontent.com/semanticbits/FedRAMP-Dashboard/master/docs/reports/Codeclimate-Test-Coverage.png "Code Climate - Test Coverage")

### HTML Sniffer
![HTML Sniffer 508 Compliance](https://raw.githubusercontent.com/semanticbits/FedRAMP-Dashboard/master/docs/reports/508-Compliance-HTML-Sniffer.png "HTML Sniffer - 508 Compliance")


### Local Reports

### JSHint
![jshint report](https://raw.githubusercontent.com/semanticbits/FedRAMP-Dashboard/master/docs/reports/jshint-report-html-1460153758627.png "jshint")

### Unit Test
![unit test report](https://raw.githubusercontent.com/semanticbits/FedRAMP-Dashboard/master/docs/reports/unit-tests-html-1460153678786.png "unit test")

### Coverage
![coverage report](https://raw.githubusercontent.com/semanticbits/FedRAMP-Dashboard/master/docs/reports/coverage-report-index-html-1460153807152.png "test coverage")
